package com.app.notes.controller;

import com.app.notes.model.ResultMap;
import com.app.notes.model.domain.NoteInfo;
import com.app.notes.service.NoteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.util.List;

@RestController
public class NoteController {

    private final NoteService noteService;

    @Autowired
    public NoteController(NoteService noteService) {
        this.noteService = noteService;
    }

    @RequestMapping(value = "/notes", method = RequestMethod.GET)
    public ResultMap login(HttpServletRequest request) {
        String clientVersion = request.getHeader("clientVersion");
        System.out.println(clientVersion);
        List<NoteInfo> list = noteService.list();
//        for (int i = 0; i < list.size(); i++) {
//            NoteInfo noteInfo = list.get(i);
//            noteInfo.setPrice(String.valueOf(noteInfo.getPage_count()* 0.1 + 1));
//            noteInfo.setUrl("http://47.96.139.220"
//                    + File.separator
//                    + "image"
//                    + File.separator
//                    + noteInfo.getCourse()
//                    + "-"
//                    + noteInfo.getCourse_index()
//                    + File.separator
//                    + i +".jpeg");
//        }
        return new ResultMap().success().message("").data(list);

    }

}
