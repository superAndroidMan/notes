package com.app.notes.controller;

import com.alibaba.fastjson.JSON;
import com.app.notes.config.MessageCode;
import com.app.notes.model.LoginDetail;
import com.app.notes.model.ResultMap;
import com.app.notes.model.TokenDetail;
import com.app.notes.model.domain.AppInfo;
import com.app.notes.model.domain.User;
import com.app.notes.model.vo.Data;
import com.app.notes.model.vo.RequestLoginUser;
import com.app.notes.service.AppInfoService;
import com.app.notes.service.LoginService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
public class LoginController {

    private final LoginService loginService;
    private final AppInfoService appInfoService;

    @Value("${token.header}")
    private String tokenHeader;

    @Autowired
    public LoginController(LoginService loginService,AppInfoService appInfoService) {
        this.loginService = loginService;
        this.appInfoService = appInfoService;
    }

    @RequestMapping(value = "/login",method = RequestMethod.POST)
    public ResultMap login(RequestLoginUser requestLoginUser, BindingResult bindingResult){
        // 检查有没有输入用户名密码和格式对不对
        if (bindingResult.hasErrors()){
            return new ResultMap().fail("400").message("缺少参数或者参数格式不对").data("");
        }
        QueryWrapper<User> wrapper = new QueryWrapper<>();
        wrapper.eq("username",requestLoginUser.getUsername());
        LoginDetail loginDetail = loginService.getOne(wrapper);
        ResultMap ifLoginFail = checkAccount(requestLoginUser, loginDetail);
        if (ifLoginFail != null){
            return ifLoginFail;
        }
        return new ResultMap().success().message("").data(new Data().addObj(tokenHeader, loginService.generateToken((TokenDetail) loginDetail)));
    }

    private ResultMap checkAccount(RequestLoginUser requestLoginUser, LoginDetail loginDetail){
        if (loginDetail == null){
            return register(requestLoginUser);
        } else {
            if (!loginDetail.enable()){
                return new ResultMap().fail(MessageCode.SERVER_ERROR_CODE).message("账号在黑名单中").data("");
            }
            if (!loginDetail.getPassword().equals(requestLoginUser.getPassword())){
                return new ResultMap().fail(MessageCode.CLIENT_ERROR_CODE).message("密码错误！").data("");
            }
        }
        return null;
    }

    private ResultMap register(RequestLoginUser requestLoginUser){
        User user = new User();
        user.setUsername(requestLoginUser.getUsername());
        user.setPassword(requestLoginUser.getPassword());
        user.setEnable('1');
        loginService.save(user);
        return new ResultMap().success().message("").data(new Data().addObj(tokenHeader, loginService.generateToken(user)));
    }

    @RequestMapping(value = "/appVersion",method = RequestMethod.GET)
    public ResultMap appVersion(HttpServletRequest request){

        String appInfoStr = request.getHeader("appInfo");
        AppInfo appInfo = (AppInfo) JSON.parse(appInfoStr);
        String clientVersion = appInfo.getClientVersion();
        AppInfo lastVersion = appInfoService.getLastVersion();
        if(lastVersion == null){
            return null;
        }
        int i = AppInfo.compareVersion(clientVersion, lastVersion.getVersion());
        if(i == -1){
            return new ResultMap().success().message("").data(lastVersion);
        }
        return null;
    }

    @RequestMapping(value = "/splash",method = RequestMethod.GET)
    public ResultMap splash(HttpServletRequest request){

        return new ResultMap().success().message("").data("https:\\/\\/wechat.423.group\\/upload\\/video\\/036451a888cb7ab9530e9e95cd02c730.jpg");
    }

}
