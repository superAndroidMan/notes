package com.app.notes.config;

public class MessageCode {

    public static final int TOKEN_OVERDUE_CODE = 4000;
    public static final int SERVER_ERROR_CODE = 3000;
    public static final int CLIENT_ERROR_CODE = 2000;
    public static final int SUCCESS_CODE = 1000;

}
