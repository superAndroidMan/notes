package com.app.notes.service;


import com.app.notes.model.domain.User;

import java.util.List;

public interface UserService {

    List<User> findAll();
}
