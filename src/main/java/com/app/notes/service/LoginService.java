package com.app.notes.service;


import com.app.notes.model.TokenDetail;
import com.app.notes.model.domain.User;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @version V1.0.0
 * @Description
 */
public interface LoginService extends IService<User> {

    String generateToken(TokenDetail tokenDetail);

}
