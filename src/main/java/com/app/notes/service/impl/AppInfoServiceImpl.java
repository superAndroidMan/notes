package com.app.notes.service.impl;

import com.app.notes.mapper.AppInfoMapper;
import com.app.notes.model.domain.AppInfo;
import com.app.notes.service.AppInfoService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

@Service
public class AppInfoServiceImpl extends ServiceImpl<AppInfoMapper, AppInfo> implements AppInfoService {

    @Override
    public AppInfo getLastVersion() {
        QueryWrapper<AppInfo> wrapper = new QueryWrapper<>();
        wrapper.last("limit 1");
        return getOne(wrapper);
    }
}
