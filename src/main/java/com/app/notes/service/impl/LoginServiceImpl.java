package com.app.notes.service.impl;

import com.app.notes.mapper.UserMapper;
import com.app.notes.model.TokenDetail;
import com.app.notes.model.domain.User;
import com.app.notes.service.LoginService;
import com.app.notes.utils.TokenUtils;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @version V1.0.0
 * @Description
 */
@Service
public class LoginServiceImpl extends ServiceImpl<UserMapper,User> implements LoginService {

    private final TokenUtils tokenUtils;

    @Autowired
    public LoginServiceImpl(TokenUtils tokenUtils) {
        this.tokenUtils = tokenUtils;
    }

    @Override
    public String generateToken(TokenDetail tokenDetail) {
        return tokenUtils.generateToken(tokenDetail);
    }
}
