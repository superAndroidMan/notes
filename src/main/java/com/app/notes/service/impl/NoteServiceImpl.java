package com.app.notes.service.impl;

import com.app.notes.mapper.NoteMapper;
import com.app.notes.model.domain.NoteInfo;
import com.app.notes.service.NoteService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

@Service
public class NoteServiceImpl extends ServiceImpl<NoteMapper, NoteInfo> implements NoteService {


}
