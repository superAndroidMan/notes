package com.app.notes.service;


import com.app.notes.model.domain.AppInfo;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @version V1.0.0
 * @Description
 */
public interface AppInfoService extends IService<AppInfo> {

    AppInfo getLastVersion();

}
