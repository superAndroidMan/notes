package com.app.notes.service;

import com.app.notes.model.domain.NoteInfo;
import com.baomidou.mybatisplus.extension.service.IService;

public interface NoteService extends IService<NoteInfo> {
}
