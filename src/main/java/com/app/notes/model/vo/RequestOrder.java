package com.app.notes.model.vo;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class RequestOrder {

    @NotNull
    private String username;

    @NotNull
    private String address;

    @NotNull
    private String phone;

    @NotNull
    private List<String> notesList;

    @NotNull
    public String getUsername() {
        return username;
    }

    public void setUsername(@NotNull String username) {
        this.username = username;
    }

    @NotNull
    public String getAddress() {
        return address;
    }

    public void setAddress(@NotNull String address) {
        this.address = address;
    }

    @NotNull
    public String getPhone() {
        return phone;
    }

    public void setPhone(@NotNull String phone) {
        this.phone = phone;
    }

    @NotNull
    public List<String> getNotesList() {
        return notesList;
    }

    public void setNotesList(@NotNull List<String> notesList) {
        this.notesList = notesList;
    }

    @Override
    public String toString() {
        return "RequestOrder{" +
                "username='" + username + '\'' +
                ", address='" + address + '\'' +
                ", phone='" + phone + '\'' +
                ", notesList=" + notesList +
                '}';
    }
}
