package com.app.notes.model;

/**
 * @version V1.0.0
 * @Description
 */
public interface LoginDetail {

    String getUsername();
    String getPassword();
    boolean enable();
}
