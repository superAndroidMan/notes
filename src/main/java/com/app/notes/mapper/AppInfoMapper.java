package com.app.notes.mapper;

import com.app.notes.model.domain.AppInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;

@Mapper
@Component
public interface AppInfoMapper extends BaseMapper<AppInfo> {
}
