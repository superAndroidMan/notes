package com.app.notes.mapper;

import com.app.notes.model.domain.NoteInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;

@Mapper
@Component
public interface NoteMapper extends BaseMapper<NoteInfo> {
}
