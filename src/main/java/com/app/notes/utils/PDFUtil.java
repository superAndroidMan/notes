package com.app.notes.utils;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.rendering.PDFRenderer;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class PDFUtil {

    public static void pdf2Image(String fileName, String targetFolder) {
        File pdfFile = new File(fileName);
        //　加载pdf文档，在pdmodel包
        try {
            PDDocument document = PDDocument.load(pdfFile);
            PDFRenderer renderer = new PDFRenderer(document);
            int pageCount = document.getNumberOfPages();
            for (int i = 0; i < pageCount; i++) {
                /**
                 * renderImage(i,1.9f)
                 *
                 * i: 指定页对象下标,从0开始,0即第一页
                 *
                 * 1.9f:DPI值(Dots Per Inch),官方描述比例因子，其中1=72 DPI
                 *      DPI是指每英寸的像素,也就是扫描精度,DPI越低,扫描的清晰度越低
                 *      根据根据自己需求而定,我导出的图片是 927x1372
                 */
                BufferedImage image = renderer.renderImage(i,1.9f);
                // 导出图片命名为:0-n.jpeg
                ImageIO.write(image, "JPEG", new File(targetFolder+i+".jpeg"));
                System.out.println("导出 "+targetFolder+i+".jpeg...");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
